const venom = require("venom-bot");

const venomInstance = venom.create();

const express = require("express");
const app = express();
app.use(express.json());

app.use((req, _res, next) => {
  req.venom = venomInstance;
  next();
});

app.post("/send-message", function (req, res) {
  try {
    const { phone, message } = req.body;
    const treatedPhone = `55${phone}@c.us`;

    req.venom
      .then(async (client) => await client.sendText(treatedPhone, message))
      .catch((erro) => {
        console.log(erro);
      });
    res.status(200);
  } catch (error) {
    console.log(error);
    res.status(500)
  }
});

app.listen(3500);
